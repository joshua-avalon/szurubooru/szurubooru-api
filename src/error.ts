import { ApiErrorResponse } from "./request";

export class HttpError extends Error {
  public status: number;
  public text: string;
  public constructor(status: number, text: string) {
    super(`${status}: ${text}`);
    this.status = status;
    this.text = text;
  }
}

export class ApiError extends Error {
  public name: string;
  public title: string;
  public description: string;

  public constructor(json: ApiErrorResponse) {
    const { name, title, description } = json;
    super(description);
    this.name = name;
    this.title = title;
    this.description = description;
  }
}
