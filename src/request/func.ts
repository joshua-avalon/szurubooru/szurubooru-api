import { Authorization, Header, HttpMethod, Query } from "./model";

const jsonHeaders: Header = {
  Accept: "application/json",
  "Content-Type": "application/json"
};

export interface RequestJsonParams {
  method: HttpMethod;
  header?: Header;
  json?: any;
}

export const requestJson = (
  path: string,
  params: RequestJsonParams,
  authorization?: Authorization,
  query: Query = {}
): Promise<Response> => {
  const { method, header, json } = params;
  const headers = { ...jsonHeaders, ...header };
  if (authorization && !headers.hasOwnProperty("Authorization")) {
    headers["Authorization"] = authorization.header;
  }
  const url = new URL(path);
  Object.keys(query)
    .filter(key => query[key] !== null && query[key] !== undefined)
    .forEach(key => url.searchParams.append(key, "" + query[key]));

  const body = json ? JSON.stringify(json) : undefined;
  return fetch(url.href, {
    method,
    headers,
    body
  });
};

const multipartHeaders: Header = {
  Accept: "application/json",
  "Content-Type": "multipart/form-data"
};

export interface RequestFormDataParams {
  method: HttpMethod;
  header?: Header;
  content: File;
  metadata?: any;
}

export const requestFormData = (
  path: string,
  params: RequestFormDataParams,
  authorization?: Authorization
): Promise<Response> => {
  const { method, header, content, metadata } = params;
  const headers = { ...multipartHeaders, ...header };
  const body = new FormData();
  body.append("content", content);
  body.append("metadata", metadata);
  if (authorization && !headers.hasOwnProperty("Authorization")) {
    headers["Authorization"] = authorization.header;
  }
  const url = new URL(path);
  return fetch(url.href, {
    method,
    headers,
    body
  });
};
