export interface ApiErrorResponse {
  name: string;
  title: string;
  description: string;
}
