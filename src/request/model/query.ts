export interface Query {
  [key: string]: string | number | boolean | null | undefined;
}
