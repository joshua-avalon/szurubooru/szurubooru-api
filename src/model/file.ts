export interface FileToken {
  token: string;
}

export interface FileContent {
  contentUrl: string;
}
