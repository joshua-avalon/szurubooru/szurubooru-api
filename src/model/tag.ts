import { Version } from "./version";

export interface Tag extends Version {
  names: string[];
  category: string;
  implications: MicroTag[];
  suggestions: MicroTag[];
  creationTime: string;
  lastEditTime: string | null;
  usages: number;
  description: string;
}

export interface MicroTag extends Pick<Tag, "names" | "category" | "usages"> {}

export interface TagSibling<T = Tag> {
  tag: T;
  occurrences: number;
}

export interface CreateTag {
  names: string[];
  category: string;
  description?: string;
  implications?: string[];
  suggestions?: string[];
}

export interface UpdateTag extends Version {
  names?: string[];
  category?: string;
  description?: string;
  implications?: string[];
  suggestions?: string[];
}

export interface MergeTag {
  removeVersion: number;
  remove: string;
  mergeToVersion: number;
  mergeTo: string;
}

export interface TagRequest {
  tagName: string;
}
