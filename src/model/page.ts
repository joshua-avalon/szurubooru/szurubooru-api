export type PageQuery = {
  limit: number;
  offset?: number;
  query?: string;
};
