import { MicroUser } from "./user";

export type Operation = "created" | "modified" | "deleted" | "merged";
export type ResourceType = "tag" | "tag_category" | "post";

export interface Snapshot {
  operation: Operation;
  type: ResourceType;
  id: string;
  user: MicroUser;
  time: string;
  data: any;
}
