export interface UnpagedResult<T = any> {
  result: T[];
}

export interface PagedResult<T = any> {
  query: string;
  offset: number;
  limit: number;
  total: number;
  result: T[];
}
