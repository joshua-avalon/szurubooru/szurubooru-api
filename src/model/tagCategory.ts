import { Version } from "./version";

export interface TagCategory extends Version {
  name: string;
  color: string;
  usages: number;
  default: boolean;
}

export interface CreateTagCategory {
  name: string;
  color: string;
}

export interface UpdateTagCategory extends Version {
  name?: string;
  color?: string;
}

export interface TagCategoryRequest {
  categoryName: string;
}
