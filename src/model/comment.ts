import { Version } from "./version";
import { MicroUser } from "./user";
import { OwnScore } from "./constant";

export interface Comment extends Version {
  id: number;
  postId: number;
  user: MicroUser;
  text: string;
  creationTime: string;
  lastEditTime: string | null;
  score: number;
  ownScore: OwnScore;
}

export interface CreateComment {
  text: string;
  postId: string;
}

export type UpdateComment = Version & {
  text: string;
};

export interface CommentRequest {
  commentId: number;
}
