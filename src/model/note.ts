export interface Note {
  text: string;
  polygon: [number, number][];
}
