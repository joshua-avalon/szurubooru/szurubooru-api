import { PagedResult, UnpagedResult } from "./result";
import { Tag, TagSibling } from "./tag";
import { ImageSearchResult, Post } from "./post";

export type Method<
  ResultType,
  Param = void,
  WrapperType extends void | PagedResult | UnpagedResult = void
> = ResultType extends TagSibling
  ? Param extends void
    ? {
        (): TagSiblingPickType<WrapperType>;
        <PickType extends keyof Tag>(...fields: PickType[]): TagSiblingPickType<
          WrapperType,
          PickType
        >;
      }
    : {
        (body: Param): TagSiblingPickType<WrapperType>;
        <PickType extends keyof Tag>(
          body: Param,
          ...fields: PickType[]
        ): TagSiblingPickType<WrapperType, PickType>;
      }
  : ResultType extends ImageSearchResult
  ? Param extends void
    ? {
        (): ImageSearchResultPickType<WrapperType>;
        <PickType extends keyof Post>(
          ...fields: PickType[]
        ): ImageSearchResultPickType<WrapperType, PickType>;
      }
    : {
        (body: Param): ImageSearchResultPickType<WrapperType>;
        <PickType extends keyof Post>(
          body: Param,
          ...fields: PickType[]
        ): ImageSearchResultPickType<WrapperType, PickType>;
      }
  : Param extends void
  ? {
      (): ResultPickType<ResultType, WrapperType>;
      <PickType extends keyof ResultType>(
        ...fields: PickType[]
      ): ResultPickType<ResultType, WrapperType, PickType>;
    }
  : {
      (body: Param): ResultPickType<ResultType, WrapperType>;
      <PickType extends keyof ResultType>(
        body: Param,
        ...fields: PickType[]
      ): ResultPickType<ResultType, WrapperType, PickType>;
    };

type ResultPick<
  ResultType,
  PickType extends keyof ResultType | void
> = PickType extends keyof ResultType ? Pick<ResultType, PickType> : PickType;

type ResultPickType<
  ResultType,
  WrapperType extends void | PagedResult | UnpagedResult,
  PickType extends keyof ResultType | void = void
> = WrapperType extends PagedResult<ResultType>
  ? Promise<PagedResult<ResultPick<ResultType, PickType>>>
  : WrapperType extends UnpagedResult<ResultType>
  ? Promise<UnpagedResult<ResultPick<ResultType, PickType>>>
  : Promise<ResultPick<ResultType, PickType>>;

type TagSiblingPick<
  PickType extends keyof Tag | void
> = PickType extends keyof Tag ? TagSibling<Pick<Tag, PickType>> : TagSibling;

type TagSiblingPickType<
  WrapperType extends void | PagedResult | UnpagedResult,
  PickType extends keyof Tag | void = void
> = WrapperType extends PagedResult<TagSibling>
  ? Promise<PagedResult<TagSiblingPick<PickType>>>
  : WrapperType extends UnpagedResult<TagSibling>
  ? Promise<UnpagedResult<TagSiblingPick<PickType>>>
  : Promise<TagSiblingPick<PickType>>;

type ImageSearchResultPick<
  PickType extends keyof Post | void
> = PickType extends keyof Post
  ? ImageSearchResult<Pick<Post, PickType>>
  : ImageSearchResult;

type ImageSearchResultPickType<
  WrapperType extends void | PagedResult | UnpagedResult,
  PickType extends keyof Post | void = void
> = WrapperType extends PagedResult<ImageSearchResult>
  ? Promise<PagedResult<ImageSearchResultPick<PickType>>>
  : WrapperType extends UnpagedResult<ImageSearchResult>
  ? Promise<UnpagedResult<ImageSearchResultPick<PickType>>>
  : Promise<ImageSearchResultPick<PickType>>;
