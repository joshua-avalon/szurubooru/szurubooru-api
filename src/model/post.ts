import { Version } from "./version";
import { MicroTag } from "./tag";
import { Note } from "./note";
import { MicroUser } from "./user";
import { Content, Thumbnail } from "./content";
import { OwnScore, PostType, Safety } from "./constant";
import { Comment } from "./comment";

export interface Post extends Version {
  id: number;
  creationTime: string;
  lastEditTime: string | null;
  safety: Safety;
  source: string | null;
  type: PostType;
  checksum: string;
  canvasWidth: number;
  canvasHeight: number;
  contentUrl: string;
  thumbnailUrl: string;
  flags: string[];
  tags: MicroTag[];
  relations: MicroPost[];
  notes: Note[];
  user: MicroUser;
  score: number;
  ownScore: OwnScore;
  ownFavorite: boolean;
  tagCount: number;
  favoriteCount: number;
  commentCount: number;
  noteCount: number;
  featureCount: number;
  relationCount: number;
  lastFeatureTime: string | null;
  favoritedBy: MicroUser[];
  hasCustomThumbnail: boolean;
  mimeType: string;
  comments: Comment[];
}

export interface MicroPost extends Pick<Post, "id" | "thumbnailUrl"> {}

export type CreatePost = Content &
  Partial<Thumbnail> & {
    tags: string[];
    safety: Safety;
    source?: string;
    relations?: number[];
    notes?: Note[];
    flags?: string[];
    anonymous: boolean;
  };

export type UpdatePost = Version &
  Partial<Content> &
  Partial<Thumbnail> & {
    tags?: string[];
    safety?: Safety;
    source?: string;
    relations?: number[];
    notes?: Note[];
    flags?: string[];
    contentUrl?: string;
    contentToken?: string;
    thumbnailUrl?: string;
    thumbnailToken?: string;
  };

export interface PostRequest {
  postId: number;
}

export interface MergePost {
  removeVersion: number;
  remove: string;
  mergeToVersion: number;
  mergeTo: string;
  replaceContent: boolean;
}

export interface FeaturePost {
  id: number;
}

export interface ImageSearchResult<T = Post> {
  exactPost: T;
  similarPosts: { post: T; distance: number }[];
}
