export interface Content {
  contentUrl?: string;
  contentToken?: string;
}

export interface Thumbnail {
  thumbnailUrl?: string;
  thumbnailToken?: string;
}
