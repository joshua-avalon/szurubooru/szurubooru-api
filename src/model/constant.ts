export type Safety = "safe" | "sketchy" | "unsafe";
export type PostType = "image" | "animation" | "video" | "flash" | "youtube";
export type OwnScore = -1 | 0 | 1;
export type Rank =
  | "restricted"
  | "regular"
  | "power"
  | "moderator"
  | "administrator";
export type AvatarStyle = "gravatar" | "manual";

export interface UpdateScore {
  score: OwnScore;
}
