import { Post } from "./post";
import { User } from "./user";
import { Rank } from "./constant";

export interface Info {
  postCount: number;
  diskUsage: number;
  featuredPost: Post | null;
  featuringTime: string | null;
  featuringUser: User;
  serverTime: string;
  config: {
    userNameRegex: string;
    passwordRegex: string;
    tagNameRegex: string;
    tagCategoryNameRegex: string;
    defaultUserRank: Rank;
    enableSafety: boolean;
    contactEmail: string | null;
    canSendMails: boolean;
    privileges: {
      [key: string]: Rank;
    };
  };
}
