import { Version } from "./version";
import { AvatarStyle, Rank } from "./constant";

export interface User extends Version {
  name: string;
  email: string | false | null;
  rank: Rank;
  lastLoginTime: string;
  creationTime: string;
  avatarStyle: AvatarStyle;
  avatarUrl: string;
  commentCount: number;
  uploadedPostCount: number;
  likedPostCount: number | false;
  dislikedPostCount: number | false;
  favoritePostCount: number;
}

export interface MicroUser extends Pick<User, "name" | "avatarUrl"> {}

export interface CreateUser {
  name: string;
  password: string;
  email?: string;
  rank?: Rank;
  avatarStyle?: AvatarStyle;
}

export interface UpdateUser extends Version {
  name?: string;
  password?: string;
  email?: string;
  rank?: Rank;
  avatarStyle?: AvatarStyle;
  avatarUrl?: string;
  avatarToken?: string;
}

export interface UserRequest {
  userName: string;
}

export interface UserToken extends Version {
  user: MicroUser;
  token: string;
  note: string;
  enabled: boolean;
  expirationTime: string | null;
  creationTime: string;
  lastEditTime: string | null;
  lastUsageTime: string | null;
}

export interface CreateUserToken {
  bumpLogin?: boolean;
  enabled?: boolean;
  note?: string;
  expirationTime?: string;
}

export interface UpdateUserToken extends Version {
  enabled?: boolean;
  note?: string;
  expirationTime?: string;
}

export interface UserTokenRequest extends UserRequest {
  userToken: string;
}

export interface SendPasswordResetRequest {
  emailOrName: string;
}

export interface ConfirmPasswordResetRequest extends SendPasswordResetRequest {
  token: string;
}

export interface PasswordReset {
  password: string;
}
