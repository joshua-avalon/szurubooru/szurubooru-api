import {
  ApiErrorResponse,
  Authorization,
  HttpMethod,
  Query,
  requestFormData,
  requestJson
} from "./request";
import { ApiError, HttpError } from "./error";
import {
  Comment,
  CommentRequest,
  ConfirmPasswordResetRequest,
  Content,
  CreateComment,
  CreatePost,
  CreateTag,
  CreateTagCategory,
  CreateUser,
  CreateUserToken,
  FeaturePost,
  FileContent,
  FileToken,
  ImageSearchResult,
  Info,
  MergePost,
  MergeTag,
  Method,
  PageQuery,
  PagedResult,
  PasswordReset,
  Post,
  PostRequest,
  SendPasswordResetRequest,
  Tag,
  TagCategory,
  TagCategoryRequest,
  TagRequest,
  TagSibling,
  UnpagedResult,
  UpdateComment,
  UpdatePost,
  UpdateScore,
  UpdateTag,
  UpdateTagCategory,
  UpdateUser,
  UpdateUserToken,
  User,
  UserRequest,
  UserToken,
  UserTokenRequest,
  Version
} from "./model";
import { Snapshot } from "./model/snapshot";

interface RequestParam {
  path: string;
  method: HttpMethod;
  query?: Query;
  fields?: string[];
  body?: any;
}

interface RequestFormParam {
  path: string;
  method: HttpMethod;
  content: File;
  metadata?: any;
}

interface MethodParam {
  fields?: string[];
  query?: Query;
  body?: any;
}

interface MethodFormParam {
  content: File;
  metadata?: any;
}

export class Szurubooru {
  private url: string;
  private authorization?: Authorization;

  public constructor(url: string) {
    this.url = url.replace(/\/$/, ""); // Remove tailing slash
  }

  public setBasicAuthorization = (user: string, password: string): void => {
    this.authorization = Authorization.basic(user, password);
  };

  public setTokenAuthorization = (user: string, token: string): void => {
    this.authorization = Authorization.token(user, token);
    const a: any = { contentUrl: "" };
    this.favoritePost({ postId: 1 });
    this.searchImage(a);
  };

  /* Tag Category */

  public listTagCategories: Method<TagCategory, void, UnpagedResult> = (
    ...fields: (keyof TagCategory)[]
  ) => this.get("/tag-categories", { fields });

  public createTagCategory: Method<TagCategory, CreateTagCategory> = (
    body: CreateTagCategory,
    ...fields: (keyof TagCategory)[]
  ) => this.post("/tag-categories", { fields, body });

  public getTagCategory: Method<TagCategory, TagCategoryRequest> = (
    request: TagCategoryRequest,
    ...fields: (keyof TagCategory)[]
  ) => {
    const { categoryName } = request;
    return this.get(`/tag-categories/${categoryName}`, { fields });
  };

  public updateTagCategory: Method<
    TagCategory,
    UpdateTagCategory & TagCategoryRequest
  > = (
    request: UpdateTagCategory & TagCategoryRequest,
    ...fields: (keyof TagCategory)[]
  ) => {
    const { categoryName, ...body } = request;
    return this.put(`/tag-categories/${categoryName}`, { fields, body });
  };

  public deleteTagCategory: Method<{}, Version & TagCategoryRequest> = (
    request: Version & TagCategoryRequest
  ) => {
    const { categoryName, ...body } = request;
    return this.delete(`/tag-categories/${categoryName}`, { body });
  };

  public defaultTagCategory: Method<TagCategory, TagCategoryRequest> = (
    request: TagCategoryRequest,
    ...fields: (keyof TagCategory)[]
  ) => {
    const { categoryName } = request;
    return this.put(`/tag-categories/${categoryName}/default`, { fields });
  };

  /* Tag */
  public listTags: Method<Tag, PageQuery, PagedResult> = (
    query: PageQuery,
    ...fields: (keyof Tag)[]
  ) => this.get("/tags", { fields, query });

  public createTag: Method<Tag, CreateTag> = (
    body: CreateTag,
    ...fields: (keyof Tag)[]
  ) => this.post("/tags", { fields, body });

  public getTag: Method<Tag, TagRequest> = (
    request: TagRequest,
    ...fields: (keyof Tag)[]
  ) => {
    const { tagName } = request;
    return this.get(`/tag/${tagName}`, { fields });
  };

  public updateTag: Method<Tag, UpdateTag & TagRequest> = (
    request: UpdateTag & TagRequest,
    ...fields: (keyof Tag)[]
  ) => {
    const { tagName, ...body } = request;
    return this.put(`/tag/${tagName}`, { fields, body });
  };

  public deleteTag: Method<{}, Version & TagRequest> = (
    request: Version & TagRequest
  ) => {
    const { tagName, ...body } = request;
    return this.delete(`/tag/${tagName}`, { body });
  };

  public mergeTag: Method<Tag, MergeTag> = (
    body: MergeTag,
    ...fields: (keyof Tag)[]
  ) => this.post("/tag-merge", { fields, body });

  public getTagSiblings: Method<TagSibling, TagRequest> = (
    request: TagRequest,
    ...fields: (keyof Tag)[]
  ) => {
    const { tagName } = request;
    return this.get(`/tag-siblings/${tagName}`, { fields });
  };

  /* Post */
  public listPosts: Method<Post, PageQuery, PagedResult> = (
    query: PageQuery,
    ...fields: (keyof Post)[]
  ) => this.get("/posts", { fields, query });

  public createPost: Method<Post, CreatePost> = (
    body: CreatePost,
    ...fields: (keyof Post)[]
  ) => this.post("/posts", { fields, body });

  public getPost: Method<Post, PostRequest> = (
    request: PostRequest,
    ...fields: (keyof Post)[]
  ) => {
    const { postId } = request;
    return this.get(`/post/${postId}`, { fields });
  };

  public updatePost: Method<Tag, UpdatePost & PostRequest> = (
    request: UpdatePost & PostRequest,
    ...fields: (keyof Tag)[]
  ) => {
    const { postId, ...body } = request;
    return this.put(`/post/${postId}`, { fields, body });
  };

  public deletePost: Method<{}, Version & PostRequest> = (
    request: Version & PostRequest
  ) => {
    const { postId, ...body } = request;
    return this.delete(`/post/${postId}`, { body });
  };

  public mergePost: Method<Post, MergePost> = (
    body: MergePost,
    ...fields: (keyof Post)[]
  ) => this.post("/post-merge", { fields, body });

  public scorePost: Method<Post, UpdateScore & PostRequest> = (
    request: UpdateScore & PostRequest,
    ...fields: (keyof Post)[]
  ) => {
    const { postId, ...body } = request;
    return this.put(`/post/${postId}/score`, { fields, body });
  };

  public favoritePost: Method<Post, PostRequest> = (
    request: PostRequest,
    ...fields: (keyof Post)[]
  ) => {
    const { postId } = request;
    return this.post(`/post/${postId}/favorite`, { fields });
  };

  public unfavoritePost: Method<Post, PostRequest> = (
    request: PostRequest,
    ...fields: (keyof Post)[]
  ) => {
    const { postId } = request;
    return this.delete(`/post/${postId}/favorite`, { fields });
  };

  public getFeaturedPost: Method<Post> = (...fields: (keyof Post)[]) => {
    return this.get("/featured-post", { fields });
  };

  public featurePost: Method<Tag, FeaturePost> = (
    body: FeaturePost,
    ...fields: (keyof Tag)[]
  ) => {
    return this.post("/featured-post", { fields, body });
  };

  public searchImage: Method<ImageSearchResult, Content> = (
    body: Content,
    ...fields: (keyof Post)[]
  ) => {
    return this.post("/posts/reverse-search", { fields, body });
  };

  /* Comment */
  public listComments: Method<Comment, PageQuery, PagedResult> = (
    query: PageQuery,
    ...fields: (keyof Comment)[]
  ) => this.get("/comments", { fields, query });

  public createComment: Method<Comment, CreateComment> = (
    body: CreateComment,
    ...fields: (keyof Comment)[]
  ) => this.post("/comments", { fields, body });

  public getComment: Method<Comment, CommentRequest> = (
    request: CommentRequest,
    ...fields: (keyof Comment)[]
  ) => {
    const { commentId } = request;
    return this.get(`/comment/${commentId}`, { fields });
  };

  public updateComment: Method<Comment, UpdateComment & CommentRequest> = (
    request: UpdateComment & CommentRequest,
    ...fields: (keyof Comment)[]
  ) => {
    const { commentId, ...body } = request;
    return this.put(`/comment/${commentId}`, { fields, body });
  };

  public deleteComment: Method<{}, Version & CommentRequest> = (
    request: Version & CommentRequest
  ) => {
    const { commentId, ...body } = request;
    return this.delete(`/comment/${commentId}`, { body });
  };

  public scoreComment: Method<Post, UpdateScore & CommentRequest> = (
    request: UpdateScore & CommentRequest,
    ...fields: (keyof Post)[]
  ) => {
    const { commentId, ...body } = request;
    return this.put(`/comment/${commentId}/score`, { fields, body });
  };

  /* User */
  public listUsers: Method<User, PageQuery, PagedResult> = (
    query: PageQuery,
    ...fields: (keyof User)[]
  ) => this.get("/users", { fields, query });

  public createUser: Method<User, CreateUser> = (
    body: CreateUser,
    ...fields: (keyof User)[]
  ) => this.post("/users", { fields, body });

  public getUser: Method<User, UserRequest> = (
    request: UserRequest,
    ...fields: (keyof User)[]
  ) => {
    const { userName } = request;
    return this.get(`/user/${userName}`, { fields });
  };

  public updateUser: Method<User, UpdateUser & UserRequest> = (
    request: UpdateUser & UserRequest,
    ...fields: (keyof User)[]
  ) => {
    const { userName, ...body } = request;
    return this.put(`/user/${userName}`, { fields, body });
  };

  public deleteUser: Method<{}, Version & UserRequest> = (
    request: Version & UserRequest
  ) => {
    const { userName, ...body } = request;
    return this.delete(`/user/${userName}`, { body });
  };

  public listUserTokens: Method<UserToken, UserRequest, UnpagedResult> = (
    request: UserRequest,
    ...fields: (keyof User)[]
  ) => {
    const { userName } = request;
    return this.get(`/user-token/${userName}`, { fields });
  };

  public createUserToken: Method<UserToken, CreateUserToken & UserRequest> = (
    request: CreateUserToken & UserRequest,
    ...fields: (keyof UserToken)[]
  ) => {
    const { userName, bumpLogin = false, ...body } = request;
    const query = bumpLogin ? { "bump-login": true } : undefined;
    return this.post(`/user-tokens/${userName}`, { fields, body, query });
  };

  public updateUserToken: Method<UserToken, UpdateUserToken & UserRequest> = (
    request: UpdateUserToken & UserRequest,
    ...fields: (keyof UserToken)[]
  ) => {
    const { userName, ...body } = request;
    return this.put(`/user-token/${userName}`, { fields, body });
  };

  public deleteUserToken: Method<{}, Version & UserTokenRequest> = (
    request: Version & UserTokenRequest
  ) => {
    const { userName, userToken, ...body } = request;
    return this.delete(`/user-token/${userName}/${userToken}`, { body });
  };

  public sendPasswordReset: Method<{}, SendPasswordResetRequest> = (
    request: SendPasswordResetRequest
  ) => {
    const { emailOrName } = request;
    return this.get(`/password-reset/${emailOrName}`);
  };

  public confirmPasswordReset: Method<
    PasswordReset,
    ConfirmPasswordResetRequest
  > = (request: ConfirmPasswordResetRequest) => {
    const { emailOrName, ...body } = request;
    return this.post(`/password-reset/${emailOrName}`, { body });
  };

  public getInfo: Method<Info> = () => this.get("/info");

  public updateFile = (content: File, metadata?: any) =>
    this.postForm("/uploads", { content, metadata }) as Promise<FileToken>;

  public updateFileUrl: Method<FileToken, FileContent> = (body: FileContent) =>
    this.post("/uploads", { body });

  public listSnapshots: Method<Snapshot, PageQuery, PagedResult> = (
    query: PageQuery
  ) => this.get("/snapshots", { query });

  /* Private */
  private getUrl = (path: string): string => {
    const rootPath = path.startsWith("/") ? path : "/" + path;
    return this.url + rootPath;
  };

  private get = (path: string, param?: MethodParam) => {
    return this.request({
      path,
      method: HttpMethod.Get,
      ...param
    });
  };

  private post = (path: string, param?: MethodParam) => {
    return this.request({
      path,
      method: HttpMethod.Post,
      ...param
    });
  };

  private put = (path: string, param?: MethodParam) => {
    return this.request({ path, method: HttpMethod.Put, ...param });
  };

  private delete = (path: string, param?: MethodParam) => {
    return this.request({ path, method: HttpMethod.Delete, ...param });
  };

  private request = (param: RequestParam) => {
    const { path, method, fields = [], query = {}, body } = param;
    const url = this.getUrl(path);
    const params = { method, body };
    const queries = {
      ...query,
      fields: fields.join(",") || undefined
    };
    const promise = requestJson(url, params, this.authorization, queries);
    return this.handleJsonResponse(promise);
  };

  private postForm = (path: string, param: MethodFormParam) => {
    return this.requestForm({
      path,
      method: HttpMethod.Post,
      ...param
    });
  };

  private requestForm = (param: RequestFormParam) => {
    const { path, method, content, metadata } = param;
    const url = this.getUrl(path);
    const params = { method, content, metadata };
    const promise = requestFormData(url, params, this.authorization);
    return this.handleJsonResponse(promise);
  };

  private handleJsonResponse = (promise: Promise<Response>): Promise<any> => {
    return promise.then(response => {
      if (!response.ok) {
        return this.handleJsonErrorResponse(response);
      }
      return response.json();
    });
  };

  private handleJsonErrorResponse = (response: Response): Promise<any> => {
    return response
      .json()
      .then((json: ApiErrorResponse) => Promise.reject(new ApiError(json)))
      .catch(() =>
        Promise.reject(new HttpError(response.status, response.statusText))
      );
  };
}
